
function timeMap (data) {

    var wWindow = $('#time-map').width() * 0.95,
        hWindow = $('#time-map').width() * 0.95,
        margin = { top: 0.04 * hWindow, right: 0.02 * wWindow, bottom: 0.1 * hWindow, left: 0.1 * wWindow},
        width = wWindow - margin.left - margin.right,
        height = hWindow - margin.top - margin.bottom;

    var maxBefore = d3.max(data, function (d) { return +d.before; }),
        maxAfter =  d3.max(data, function (d) { return +d.after; }),
        max = d3.max([maxBefore, maxAfter], function (d) {return d});
        extraRange =  max * 0.05;

    var x = d3.scale.linear()
        .domain([ -extraRange , max + extraRange])
        .range([1, width]);

    var y = d3.scale.linear()
        .domain([ -extraRange , max + extraRange])
        .range([height, 0]);

    var chart = d3.select('#time-map')
        .append('svg')
        .attr('width', width + margin.right + margin.left)
        .attr('height', height + margin.top + margin.bottom)
        .attr('class', 'chart')
        .call(d3.behavior.zoom().on("zoom", function () {
            main.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" +  d3.event.scale + ")");
            d3.select(this).selectAll("circle").attr("r", wWindow * 0.015 / d3.event.scale);
            d3.select(this).selectAll("path").attr("transform", "scale( "+  1 / d3.event.scale + ")" );
            //d3.select(this).selectAll("text")
             //   .attr("font-size", 15 / d3.event.scale + "px" )
             //   .attr("transform", "translate(" + 5 /  d3.event.scale + ",0)");
            //d3.select(this).selectAll("line").attr("stroke-width", 2 / d3.event.scale);
        }));


    var main = chart.append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .attr('width', width)
        .attr('height', height)
        .attr('class', 'main');


    // Draw rectangles
    // Slowing down
    main.append("rect")
        .attr("x", x.range()[0])
        .attr("y", y.range()[1])
        .attr('width',  x(50) - x.range()[0])
        .attr('height', y(50) - y.range()[1] )
        .attr('fill', 'rgba(222,235,247,0.5)')
        .attr("stroke","lightgrey")
        .attr("stroke-width", 2.5);
    // Slow & steady
    main.append("rect")
        .attr("x", x(50) )
        .attr("y", y.range()[1])
        .attr('width',  x.range()[1] - x(50))
        .attr('height', y(50) - y.range()[1] )
        .attr('fill', 'rgba(186,228,179,0.5)')
        .attr("stroke","lightgrey")
        .attr("stroke-width", 2.5);
    // Fast & steady
    main.append("rect")
        .attr("x", x.range()[0] )
        .attr("y", y(50))
        .attr('width',  x(50) - x.range()[0])
        .attr('height', y.range()[0]  - y(50) )
        .attr('fill', 'rgba(254,204,92,0.5)')
        .attr("stroke","lightgrey")
        .attr("stroke-width", 2.5);
    // Speeding up
    main.append("rect")
        .attr("x", x(50) )
        .attr("y", y(50))
        .attr('width',  x.range()[1] - x(50))
        .attr('height', y.range()[0]  - y(50) )
        .attr('fill', '#ffffb2')
        .attr("stroke","lightgrey")
        .attr("stroke-width", 2);

    // Draw the Grid
    main.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .call(make_x_axis()
            .tickSize(-height, 0, 0)
            .tickFormat(""));

    main.append("g")
        .attr("class", "grid")
        .call(make_y_axis()
            .tickSize(-width, 0, 0)
            .tickFormat(""));


    // Add the titles  in the rectangle
    main.append("text")
        .attr("text-anchor", "middle")
        .attr("transform", "translate("+ x(25) +","+ y(55) +")")
        .text("Slowing down");

    main.append("text")
        .attr("text-anchor", "middle")
        .attr("transform", "translate("+ x(25) +","+ y(43) +")")
        .text("Fast & steady");

    main.append("text")
        .attr("text-anchor", "middle")
        .attr("transform", "translate("+ x(75) +","+ y(55) +")")
        .text("Slow & steady");

    main.append("text")
        .attr("text-anchor", "middle")
        .attr("transform", "translate("+ x(75) +","+ y(43) +")")
        .text("Speeding up");


    var g = main.append("svg:g");

    // Drawing lines
    var color = d3.interpolateLab("rgb(120,198,121)", "rgb(251,106,74)");

    var line = d3.svg.line()
        .x(function(d){return x(+d.before);})
        .y(function(d){return y(+d.after);})
        .interpolate("linear");


    // Drawing Circle
    var nodes = g.append("g")
        .attr("class", "nodes")
        .selectAll("scatter-dots")
        .data(data)
        .enter().append("g")
        .attr("transform", function(d, i) {
            d.x = x(+d.before);
            d.y = y(+d.after);
            return "translate(" + d.x + "," + d.y + ")";
        });


    var leaf = function(d, param) {
        var s = wWindow * 0.015;
        if (d[param] == "1") {
            return "M 0 0 q" + s +" "+ -s +" "+ 2*s+" 0 q "+ -s +" "+ s +" "+ -2*s +" 0";
        }
    };

    nodes.append("path")
        .attr("d", function (d) { return leaf(d, "ae") });

    nodes.append("path")
        .attr("d", function (d) { return leaf(d, "tr") })
        .attr("transform", "rotate(-45)");

    nodes.append("path")
        .attr("d", function (d) { return leaf(d, "su") })
        .attr("transform", "rotate(-90)");

    nodes.append("path")
        .attr("d", function (d) { return leaf(d, "lab") })
        .attr("transform", "rotate(-135)");

    nodes.append("path")
        .attr("d", function (d) { return leaf(d,"bio") })
        .attr("transform", "rotate(-180)");

    nodes.append("path")
        .attr("d", function (d) { return leaf(d,"scan") })
        .attr("transform", "rotate(-225)");

    nodes.append("path")
        .attr("d", function (d) { return leaf(d,"visits") })
        .attr("transform", "rotate(-270)");

    nodes.append("path")
        .attr("d", function (d) { return leaf(d,"mooc") })
        .attr("transform", "rotate(-315)");



    // Add a circle element to the previously added g element.
    nodes.append("circle")
        .attr("class", "node")
        .attr("r", function(d,i){
            return wWindow * 0.015;
        });

    // Add a text element to the previously added g element.
    nodes.append("text")
        .attr("text-anchor", "middle")
        .attr("y", ".3em")
        .text(function(d, i) {
            return i+1;
        });

    // Add the titles to the axes
    main.append("text")
        .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate("+ (-margin.left/2) +","+(height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
        .text("Time After");

    main.append("text")
        .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate("+ (width/2) +","+(height + (margin.bottom/2))+")")  // centre below axis
        .text("Time Before");


    // Add interaction
    $('#time-map-hide-lines').click(function(d){
       var clicked =  $('#time-map-hide-lines').prop("checked");
        if (clicked) {
            d3.selectAll('path').attr('style','opacity:1');
        } else {
            d3.selectAll('path').attr('style','opacity:0');
        }
    });


    // Functions for grid
    function make_x_axis() {
        return d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(10)
    }

    function make_y_axis() {
        return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(10)
    }

}
