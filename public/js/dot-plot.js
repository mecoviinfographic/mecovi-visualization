
function dotPlot(data){

    // Create structure
    var label = ['2012', '2013', '2014'],
        color = d3.scale.ordinal()
            .domain(["Mild", "Moderate", "Severe"])
            .range(["#ffffcc", "#feb24c" , "#e31a1c"]);


        d3.select("#dot-plot")
            .append('div')
            .attr('class', 'col-sm-12')
            .attr('id', 'dot-container')
            .attr('style', "text-align: center");

        var size = $("#dot-plot").width();

    /*
        d3.select("#dot-container")
            .append('div')
            .attr('class', 'row label-dot-plot')
            .append("p")
            .text(d);
    */

        d3.select("#dot-container")
            .append('div')
            .attr('class', 'row dot-box')
            .attr('id', 'dot-row')
            .attr('height', size + "px")
            .attr('width', size + "px");


    var currYear = 0;
    data.forEach(function(d,i){

        var ae = new Date(d.aeStart).getFullYear();
        if (ae == currYear) {
            drawCircle(d);
        } else {

            var svg = d3.select("#dot-row")
                .append("svg")
                .attr("width", 10)
                .attr("height", 50);

            svg.append("text")
                .attr("x", 63)
                .attr("y", 10)
                .attr("text-anchor", "start")
                .attr("transform", "translate(0,90) rotate(-90)")
                .style("font-size", "12px")
                .text(ae);

            svg.append("rect")
                .attr("x", 4)
                .attr("y", 30)
                .attr("width", 3)
                .attr("height", 25)
                .attr("fill", "back" );

            drawCircle(d);
            currYear = ae;

        }
    });


    function drawCircle (d, tip) {

        var description = _aeLabel(d.ae, d.aeStart, d.aeEnd),
            treatment = _aeLabel(d.tr, d.trStart, d.trEnd),

            tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html( "<table id=" + d.id + " class='table dot-plot-tip'>"
                    + "<tbody>"
                    + "<tr>"
                    + "<td><b>Description:</b></td>"
                    + "<td>" + description + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td><b>Treatment:</b></td>"
                    + "<td>" + treatment + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td><b>Severity:</b></td>"
                    + "<td>" + d.severity + "</td>"
                    + "</tr>"
                    + "</tbody>"
                    + "</table>"
            );

        d3.select("#dot-row")
            .append("svg")
            .attr("width", 20)
            .attr("height", 20)
            .call(tip)
            .append("circle")
            .attr("class", "circle-dot-plot")
            .attr("r", 8)
            .attr("cy",10)
            .attr("cx",10)
            .attr("fill", color(d.severity) )
            .attr("stroke", "grey")
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide);
    }

    //var graph = d3.select('#dot-row-1')

}