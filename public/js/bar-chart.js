

function barchart (dataset) {

    var w = parseInt(d3.select('#bar-chart').style('width'),10);

    var margin = {top: w/20, right: w/20, bottom: w/10, left: w/10},
        width = parseInt(d3.select('#bar-chart').style('width'), 10) - margin.left - margin.right,
        height = (parseInt(d3.select('#bar-chart').style('width'), 10) * .3) - margin.top - margin.bottom;

    var x0 = d3.scale.ordinal()
        .rangeRoundBands([0, width], .05);

    var x1 = d3.scale.ordinal();

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = d3.scale.ordinal()
        .range(["#ffffcc", "#feb24c" , "#e31a1c"]);

    var xAxis = d3.svg.axis()
        .scale(x0)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format(".1s"))
        .ticks(5);

    var svg = d3.select('#bar-chart').append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    var options = d3.keys(dataset[0]).filter(function(key) { return key !== "year"; });

    dataset.forEach(function(d) {
        d.valores = options.map(function(name) { return {name: name, value: +d[name]}; });
    });

    x0.domain(dataset.map(function(d) { return d.year; }));
    x1.domain(options).rangeRoundBands([0, x0.rangeBand()]);
    y.domain([0, d3.max(dataset, function(d) { return d3.max(d.valores, function(d) { return d.value; }); })]);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Frequency")
        .style("font-size", "12px");

    var bar = svg.selectAll(".bar")
        .data(dataset)
        .enter().append("g")
        .attr("class", "rect")
        .attr("transform", function(d) { return "translate(" + x0(d.year) + ",0)"; });

    bar.selectAll("rect")
        .data(function(d) { return d.valores; })
        .enter().append("rect")
        .attr("width", x1.rangeBand())
        .attr("x", function(d) { return x1(d.name); })
        .attr("y", function(d) { return y(d.value); })
        .attr("value", function(d){return d.name;})
        .attr("height", function(d) { return height - y(d.value); })
        .style("fill", function(d) { return color(d.name); })
        .attr("stroke", "grey");

}