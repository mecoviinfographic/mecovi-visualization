
$(document).ready(function(){


    // --- Action that happen on index.html ---- //
    // Show/Hide Time-map
    $("#show-time-map").click(function () {
        $("#time-map, #time-map-help").toggle();
    });

    // Show/Hide Dot-plot description
    $("#show-dot-plot-description").click(function () {
        $("#dot-plot-description").toggle();
    });



    // Global variables
    var patid, aeid;

    // Datasets
    var adslSel, dataSel = [];

    d3.csv("data/adsl.csv", function(adsl){
    d3.csv("data/adae.csv", function(adae) {
    d3.csv('data/timeMap.csv', function (events) {
    d3.csv('data/adlab.csv', function (adlab) {
    d3.csv('data/medscan.csv', function (scans) {
    d3.csv('data/medvisits.csv', function (visits) {
    d3.csv('data/adtr.csv', function (adtr) {
    d3.csv('data/adae-barchart.csv', function (adaebar) {


        _fillListPatients();
        _trigger();

        $(".listPatients").on("click", function () {
            var me = this;
            _trigger(me);
        });


        // FUNCTIONS
        //----------------------------------------
        function _trigger(me) {

            patid = typeof me == "undefined" ? 'ovarium01' : d3.select(me).attr('id');

            adslSel = adsl.filter(function (d) {
                return d.id == patid;
            });


            _fillTableAdsl(); // <-- dani!!
            //if ( $("#time-map").length > 0) {
                _drawTimeMap();
                _interactionTimeMap();
            //} else {
                _drawTimeline();
                _plotMedicalHistory();
                _plotLabResults();
            //}
            _drawDotPlot();

            // Styling timeline
            $(".history").parent().parent().css("background-color", "#eee");
            $(".medication").parent().parent().css("background-color", "#eee");
            $(".lab").parent().parent().css("background-color", "#eee");
            $(".medical-scan").parent().parent().css("background-color", "#eee");
            $(".medical-visit").parent().parent().css("background-color", "#eee");
        }

        //----------------------------------------
        function _fillListPatients() {

            d3.select("#select-patient")
                .selectAll('li')
                .data(adsl).enter()
                .append('li')
                .classed('listPatients', true)
                .attr('id', function (d) {
                    return d.id;
                })
                .append('a')
                .attr('href', '#')
                .text(function (d) {
                    return "Patient ID: " + d.id;
                });
        }

        //----------------------------------------
        function _fillTableAdsl() {

            d3.select("#tpatientid").text(adslSel['0'].id + "(" + adslSel['0'].initials + ")" );
            d3.select("#tdetails").text(adslSel['0'].gender + " / " + adslSel['0'].age + " / " + adslSel['0'].race);

        }

        //----------------------------------------
        function _drawTimeMap() {

            $('#time-map').empty();

            // Filter hte patient select
            var timeMapData = events.filter(function (d) {
                return d.patient == adslSel['0'].id; // <-- Select the patient
            });
            timeMap(timeMapData);
        }

        //----------------------------------------
        function _drawDotPlot() {

            $('#dot-plot').empty();

            // Filter hte patient select
            var dotplot = adae.filter(function (d) {
                return d.patient == adslSel['0'].id; // <-- Select the patient
            });
            barchart(adaebar); // <-- added barchart dataset, no filter required in this version
            dotPlot(dotplot);
        }

        //----------------------------------------
        function _drawTimeline() {

            dataSel = []; // <-- remove previous selection

            // AEs
            adae.filter(function (d) {
                return d.patient == adslSel['0'].id; // <-- Select the patient
            }).forEach(function (d, i) {  // <-- Create the de Array fro the timeline
                d.id = "ae" + i;
                d.content = _aeContent(d.id, d.ae, d.severity, d.aeEnd);
                d.start = d.aeStart;
                d.group = "mh";

                if (typeof d != 'undefined') {
                    dataSel.push(d);
                }
            });


             // Medication
             adtr.forEach(function (d, i) {  // <-- Create the de Array fro the timeline

             if (d.tr != "" && d.trStart != "NA") {

                 d.id = "tr" + i;
                 d.content = _trContent(d.id, d.tr);
                 d.start = d.trStart;
                 d.end = d.trEnd;
                 d.group = "med";

                 d = _checkItem(d);

                 if (typeof d != 'undefined') {
                    dataSel.push(d);
                 }
             }

             });

             // Lab Results
             adlab.filter(function (d) {
             return d.patient == adslSel['0'].id; // <-- Select the patient
             }).forEach(function (d, i) {

             d.id = "lb" + i;
             d.content = _lbContent(d.id, d.status);
             d.start = d.date;
             d.group = "lb";

             if (typeof d != 'undefined') {
             dataSel.push(d);
             }

             });

            // Medical scans
            scans.forEach(function (d, i) {
                var medscan = {
                    id: "ms" + i,
                    content: _msContent("ms" + i),
                    start: d.start,
                    group: "ms",
                    number: d.number
                };
                dataSel.push(medscan);
            });

            // Medical visits
            visits.forEach(function (d, i) {
                d.id = "mv" + i;
                d.content = _mvContent("mv" + i);
                d.group = "mv";
                dataSel.push(d);
            });


            // Calling  the visualization (vis.js object)
            $("#visualization").empty(); // <-- remove the content inside the div
            $('#details-div').empty();
            var container = document.getElementById('visualization'), // <-- DOM element where the Timeline will be attached
                groups = new vis.DataSet([
                    {id: 'mh', content: 'Medical History'},
                    {id: 'med', content: 'Medication'},
                    {id: 'lb', content: "Lab Results"},
                    {id: 'ms', content: "Imaging"},
                    {id: 'mv', content: "Visits"}
                ]),
                items = new vis.DataSet(dataSel),
                options = {
                    zoomMax: 157680000000 / 4.5,
                    zoomMin: 604800000,
                    showCurrentTime: false,
                    start: "2014-01-01"
                }, // <-- Configuration for the Timeline
                timeline = new vis.Timeline(container, items, groups, options); // <-- Create a Timeline

            // Draw the div
            $(".vis-item").on("click", function () {
                aeid = d3.select(this).select('tr').attr('id');
                // Filter the ae selected
                var selObject = dataSel.filter(function (d) {
                    return d.id == aeid;
                });
                // Draw the div with the information
                _drawAeDetail(selObject);
            });

        }

        //----------------------------------------
        function _drawAeDetail(selObject) {

            //$("html, body").animate({scrollTop: $(document).height()}, 1000);

            $('#details-div').empty(); // <-- Clear the detail

            if (selObject[0].id.indexOf('lb') > -1) {

                _labPlot(selObject, '#details-div');

            } else if (selObject[0].id.indexOf('ms') > -1) {

                console.log("works", selObject[0].number);
                $.post('/test' + selObject[0].number)

            } else if (selObject[0].id.indexOf('mv') > -1) {

                console.log("a." + selObject[0].description);

                $("a." + selObject[0].description).colorbox({width: "70%", open: true, opacity: 0.5, rel: 'group1', current: false});

            } else {

                <!-- table patient profile -->
                d3.select('#details-div')
                    .html(_tableAe(selObject));


                $(".close-details").on('click', function () {
                    console.log('ola');
                    d3.select(this.parentNode).remove();
                });
            }
        }

        //----------------------------------------
        function _tableAe(ae) {

            var description = _aeLabel(ae[0].ae, ae[0].aeStart, ae[0].aeEnd),
                treatment = _aeLabel(ae[0].tr, ae[0].trStart, ae[0].trEnd),

                table = "<button type='button' class='close-details btn btn-primary btn-xs'>close</button>"
                    + "<table id=" + ae.id + " class='table table-hover'>"
                    + "<tbody>"
                    + "<tr>"
                    + "<td><b>Description:</b></td>"
                    + "<td>" + description + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td><b>Treatment:</b></td>"
                    + "<td>" + treatment + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td><b>Intercurrent illness:</b></td>"
                    + "<td>" + ae[0].intercurrent + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td><b>Severity:</b></td>"
                    + "<td>" + ae[0].severity + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td><b>Outcome:</b></td>"
                    + "<td>" + ae[0].outcome + "</td>"
                    + "</tr>"
                    + "</tbody>"
                    + "</table>";

            return table;
        }

        //----------------------------------------
        function _plotMedicalHistory() {

            var mhSel = adae.filter(function (d) {
                return d.patient == adslSel['0'].id; // <-- Select the patient
            });

            for (var i = mhSel.length - 3; i < mhSel.length; i++) {

                <!-- table patient profile -->
                d3.select("#medical-history")
                    .append("div")
                    .attr('class', "col-sm-4")
                    .html(_tableAe([mhSel[i]]));
            }

        }

        //----------------------------------------
        function _plotLabResults() {

            var count = 1;
            var lbSel = adlab.filter(function (d) {
                return d.patient == adslSel['0'].id; // <-- Select the patient
            });

            for (var i = lbSel.length - 3; i < lbSel.length; i++) {

                _labPlot([lbSel[i]], '#lab-results' + count);
                count++;

            }

        }

        function _interactionTimeMap() {

            // -- Interaction -- //

            d3.select('#time-map').selectAll("circle").on("click", function(d){
                $("a." + d.doc).colorbox({width: "70%", open: true, opacity: 0.5, rel: 'group1', current: false});
            });

            d3.select('#time-map').selectAll(".su").on("click", function(d){
                $("a." + d.doc).colorbox({width: "70%", open: true, opacity: 0.5, rel: 'group1', current: false});
            });

            d3.select('#time-map').selectAll(".bio").on("click", function(d){
                $("a." + d.doc).colorbox({width: "70%", open: true, opacity: 0.5, rel: 'group1', current: false});
            });

            d3.select('#time-map').selectAll(".scan").on("click", function(d){
                $("a." + d.doc).colorbox({width: "70%", open: true, opacity: 0.5, rel: 'group1', current: false});
            });

            d3.select('#time-map').selectAll(".visits").on("click", function(d){
                $("a." + d.doc).colorbox({width: "70%", open: true, opacity: 0.5, rel: 'group1', current: false});
            });

            d3.select('#time-map').selectAll(".moc").on("click", function(d){
                $("a." + d.doc).colorbox({width: "70%", open: true, opacity: 0.5, rel: 'group1', current: false});
            });

            d3.select('#time-map').selectAll(".ae").on("click", function(d){
                $('#modal-div').empty();
                modal.style.display = "block";

                adae.filter(function(e){
                    return e.aeStart == d.date;
                }).forEach(function(e){
                    d3.select('#modal-div').append("div").html(_tableAe([e]));

                    $(".close-details").on('click', function () {
                        d3.select(this.parentNode).remove();
                    });
                });

            });

            d3.select('#time-map').selectAll(".tr").on("click", function(d){
                $('#modal-div').empty();
                modal.style.display = "block";

                adtr.filter(function(e){
                    return e.trStart == d.date;
                }).forEach(function(e){
                    d3.select('#modal-div').append("div").html(_tableAe([e]));

                    $(".close-details").on('click', function () {
                        d3.select(this.parentNode).remove();
                    });
                });
            });


            d3.select('#time-map').selectAll(".lab").on("click", function(d){
                $('#modal-div').empty();
                modal.style.display = "block";

                adlab.filter(function(e){
                    console.log(e.date, d.date, e.date == d.date);
                    return e.date == d.date;
                }).forEach(function(e){
                    console.log(e);
                    _labPlot([e], '#modal-div');
                });
            });


        }

        //----------------------------------------
    }); // <-- close adtr
    }); // <-- close medvisits
    }); // <-- close medscan
    }); // <-- close adlab
    });// <-- close timeMap
    }); // <-- close adsl
    }); //<-- close adae
    }); //<-- close adae-barchart

}); //<-- close ready jquery


// Additional functions
function sourceImg (gender) {
    if (gender == "Male"){
        return "img/male.svg";
    } else if (gender == "Female") {
        return "img/female.svg";
    }
}

function _aeLabel (label, start, stop) {
    var res;

    if (label != "" && start != "NA" && stop != "NA") {
        res = label + "<br>(from " + start + " to " + stop + ")";
    } else if (label != "" && start != "NA") {
        res = label + "<br>(from " + start + ")";
    } else if (label != "") {
        res = label;
    } else {
        res = "Not Available";
    }

    return res;
}


function _aeContent (id, ae, severity, end){
    var img,
        imgOpen = ["img/open-book-mild.svg", "img/open-book-moderate.svg", "img/open-book-severe.svg" ],
        imgClosed = ["img/closed-book-mild.svg", "img/closed-book-moderate.svg", "img/closed-book-severe.svg" ],
        sizeOpen = ['31px','25px'],
        sizeClosed = ['31px','20px'];

    if (severity == 'Mild') {
        img = end == "NA" ? imgOpen[0] : imgClosed[0];
        size = end == "NA" ? sizeOpen : sizeClosed;
    } else if (severity == "Moderate") {
        img = end == "NA" ? imgOpen[1] : imgClosed[1];
        size = end == "NA" ? sizeOpen : sizeClosed;
    } else if (severity == "Severe") {
        img = end == "NA" ? imgOpen[2] : imgClosed[2];
        size = end == "NA" ? sizeOpen : sizeClosed;
    }

    var res = "<table class='history'>"
            +   "<tr id=" + id + ">"
            +   "<td><img src=" + img + " width="+ size[0] + " height=" + size[1] + "></td>"
            +   "<td>" + ae + "</td>"
            +   "</tr>"
            + "</table>";
    return res;
}

function _trContent (id, tr){
    var res = "<table class='medication'>"
        +   "<tr id=" + id + ">"
        +   "<td><img src=img/medication.svg width='31' height='20'></td>"
        +   "<td>" + tr + "</td>"
        +   "</tr>"
        + "</table>";
    return res;
}

function _lbContent (id, status){
    var img;

    if (status == "0") {
        img = "img/lab-normal.svg";
    } else if (status == "1") {
        img = "img/lab-moderate.svg";
    } else if (status == "2") {
        img = "img/lab-severe.svg";
    }

    var res = "<table class='lab'>"
        +   "<tr id=" + id + ">"
        +   "<td><img src=" + img + " width='35' height='25'></td>"
        +   "</tr>"
        + "</table>";
    return res;
}

function _msContent (id){
    var img;

    var res = "<table class='medical-scan'>"
        +   "<tr id=" + id + ">"
        +   "<td><img src='img/medical-scan.svg' width='40' height='30'></td>"
        +   "</tr>"
        + "</table>";
    return res;
}

function _mvContent (id){
    var img;

    var res = "<table class='medical-visit'>"
        +   "<tr id=" + id + ">"
        +   "<td><img src='img/medical-visit.svg' width='35' height='25'></td>"
        +   "</tr>"
        + "</table>";
    return res;
}

// Check elements
function _checkItem (e){
    if (e.end == "NA" || e.end == e.start) {
        delete e.end;
    }
    return e;
}

