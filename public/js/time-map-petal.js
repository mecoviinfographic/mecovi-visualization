
function timeMap (data) {


    var defaultValue = [new Date(2014,1,1), new Date(2014,12,11)];

    var wWindow = $('#time-map').width() * 0.95,
        hWindow = $('#time-map').width() * 0.95,
        margin = { top: 0.04 * hWindow, right: 0.02 * wWindow, bottom: 0.1 * hWindow, left: 0.1 * wWindow},
        width = wWindow - margin.left - margin.right,
        height = hWindow - margin.top - margin.bottom;

    var maxBefore = d3.max(data, function (d) { return +d.before; }),
        maxAfter =  d3.max(data, function (d) { return +d.after; }),
        max = d3.max([maxBefore, maxAfter], function (d) {return d}),
        extraRange =  max * 0.007,
        circleSize = wWindow * 0.0075,
        leafSize = wWindow * 0.015;

    var x = d3.scale.log()
        .domain([ 1 , max + 100 * extraRange])
        .range([1, width]);

    var y = d3.scale.log()
        .domain([ 1 , max + 100 * extraRange])
        .range([height, 1]);

    var chart = d3.select('#time-map')
        .append('svg')
        .attr('width', width + margin.right + margin.left)
        .attr('height', height + margin.top + margin.bottom)
        .attr('class', 'chart');
        //.call(d3.behavior.zoom().on("zoom", function () {
            //main.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" +  d3.event.scale + ")");
            //d3.select(this).selectAll("circle").attr("r", circleSize / d3.event.scale);
            //d3.select(this).selectAll("path").attr("transform", "scale( "+  1 / d3.event.scale + ")" );
        //}))

    var main = chart.append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .attr('width', width)
        .attr('height', height)
        .attr('class', 'main');

    // Draw Axis
    main.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickFormat(function(d) { return d ; })
        );

    main.append("g")
        .attr("class", "axis")
        .call(d3.svg.axis()
            .scale(y)
            .orient("left")
            .tickFormat(function(d) { return d ; })
        );

    // Add the titles to the axes
    main.append("text")
        .attr("text-anchor", "end")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate("+ (-margin.left / 2 ) +","+ 0 +")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
        .text("Time After (Num. of days in log. scale) ");

    main.append("text")
        .attr("text-anchor", "end")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate("+ (width) +","+(height + (margin.bottom/2))+")")  // centre below axis
        .text("Time Before (Num. of days in log. scale)");


    // Visual elements
    var g = main.append("svg:g");


    // Add arrows
    g.append("svg:defs").append("svg:marker")
        .attr("id", "triangle")
        .attr("refX", 40)
        .attr("refY", 18)
        .attr("markerWidth", 30)
        .attr("markerHeight", 30)
        .attr("orient", "auto")
        .append("path")
        .attr("d", "M 12 12 24 18 12 24 15 18")
        .style("fill", "red");

    g.selectAll("lines")
        .data(data.filter(function(d){ return d["a-before"] != "NA"; }))
        .enter()
        .append("line")
        .attr("x1", function(d){ return x(+d.before + extraRange); })
        .attr("y1", function(d){ return y(+d.after + extraRange ); })
        .attr("x2", function(d){ return x(+d["a-before"] + extraRange); })
        .attr("y2", function(d){ return y(+d["a-after"] + extraRange); })
        .attr("stroke", "grey")
        .attr("stroke-width", 1)
        .attr("marker-end", "url(#triangle)");

    // Adding nodes
    var nodes = g.append("g")
        .attr("class", "nodes")
        .selectAll("scatter-dots")
        .data(data)
        .enter().append("g")
        .attr("transform", function(d, i) {
            d.x = x(+d.before + extraRange);
            d.y = y(+d.after + extraRange);
            return "translate(" + d.x + "," + d.y + ")";
        });

    var leaf = function(d, param) {
        var s = leafSize;
        if (d[param] == "1") {
            return "M 0 0 q" + s +" "+ -s +" "+ 2*s+" 0 q "+ -s +" "+ s +" "+ -2*s +" 0";
        }
    };

    nodes.append("path")
        .attr("class", "leaf ae")
        .attr("d", function (d) { return leaf(d, "ae") })
        .attr("fill", "#1b9e77")
        .attr("transform", "rotate(0)");

    nodes.append("path")
        .attr("class", "leaf tr")
        .attr("d", function (d) { return leaf(d, "tr") })
        .attr("fill", "#d95f02")
        .attr("transform", "rotate(-45)");

    nodes.append("path")
        .attr("class", "leaf su")
        .attr("d", function (d) { return leaf(d, "su") })
        .attr("fill", "#7570b3")
        .attr("transform", "rotate(-90)");

    nodes.append("path")
        .attr("class", "leaf lab")
        .attr("d", function (d) { return leaf(d, "lab") })
        .attr("fill", "#e7298a")
        .attr("transform", "rotate(-135)");

    nodes.append("path")
        .attr("class", "leaf bio")
        .attr("d", function (d) { return leaf(d,"bio") })
        .attr("fill", "#66a61e")
        .attr("transform", "rotate(-180)");

    nodes.append("path")
        .attr("class", "leaf scan")
        .attr("d", function (d) { return leaf(d,"scan") })
        .attr("fill", "#e6ab02")
        .attr("transform", "rotate(-225)");

    nodes.append("path")
        .attr("class", "leaf visits")
        .attr("d", function (d) { return leaf(d,"visits") })
        .attr("fill", "#a6761d")
        .attr("transform", "rotate(-270)");

    nodes.append("path")
        .attr("class", "leaf moc")
        .attr("d", function (d) { return leaf(d,"mooc") })
        .attr("fill", "#666666")
        .attr("transform", "rotate(-315)");


    // Add a circle element to the previously added g element.
    nodes.append("circle")
        .attr("class", "leaf")
        .attr("class", "node")
        .attr("r", circleSize)
        .attr("stroke", "grey")
        .attr("id", function(d){ return d.date});

    // Add interaction
    $('#time-map-hide-lines').click(function(d){
        var clicked =  $('#time-map-hide-lines').prop("checked");
        if (clicked) {
            d3.selectAll('path').attr('style','opacity:1');
        } else {
            d3.selectAll('path').attr('style','opacity:0');
        }
    });


    // -- Slider  -- //
    // Default filter for the slider
    var opacity = function (d) {
        var date = new Date (d.date);
        if (date >= defaultValue[0] && date <= defaultValue[1]) {
            return 1;
        } else {
            return 0;
        }
    };

    nodes.selectAll("circle").style("opacity", opacity);
    nodes.selectAll("path").style("opacity", opacity);
    g.selectAll("line").style("opacity", opacity);

    // Calling slider
    d3.select('#slider').call(d3.slider().scale(d3.time.scale().domain([new Date(2011,1,1), new Date(2015,1,1)])).axis(d3.svg.axis()).value( defaultValue ).on("slide", function(evt, value) {

        var opacity = function (d) {
            var date = new Date (d.date);
            if (date >= value[0] && date <= value[1]) {
                return 1;
            } else {
                return 0;
            }
        };

        nodes.selectAll("circle").style("opacity", opacity);
        nodes.selectAll("path").style("opacity", opacity);
        g.selectAll("line").style("opacity", opacity);

    }));

}