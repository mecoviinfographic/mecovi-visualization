

var w = parseInt(d3.select("#lb-par-coord").style('width'),10);

var margin = {top: w*0.032, right: w*0.021, bottom: w*0.011, left: w*0.011},
    width = w - margin.left - margin.right,
    height = w*0.52 - margin.top - margin.bottom;

var x = d3.scale.ordinal().rangePoints([0, width], 1),
    y = {};

var line = d3.svg.line(),
    axis = d3.svg.axis().orient("left"),
    background,
    foreground;

var svg = d3.select("#lb-par-coord").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");



d3.csv("data/ADLB-parcoord.csv", function(error, data) {


    // Extract the list of dimensions and create a scale for each.
    x.domain(dimensions = d3.keys(data[0]).filter(function(d) {
        return d != "names" && (y[d] = d3.scale.linear()
                .domain([
                    d3.min([d3.min(data, function(p) { return +p[d]; }),0]),
                    d3.max([d3.max(data, function(p) { return +p[d]; }),100])
                ]).range([height, 0]));
    }));


    // Add a group element for each dimension.
    var g = svg.selectAll(".dimension")
        .data(dimensions)
        .enter().append("g")
        .attr("class", "dimension")
        .attr("transform", function(d) { return "translate(" + x(d) + ")"; });


     g.append("rect")
     .attr("width",30)
     .attr("height", function(d){
        return d3.max([y[d](115),y[d](y[d].domain()[1])]) - y[d](y[d].domain()[1]) })
     .attr("x",-15)
     .attr("y", function(d){
        return y[d](y[d].domain()[1]) })
     .style("fill", "#e31a1c");


    g.append("rect")
        .attr("width",30)
        .attr("height", function(d){
            return y[d](100) - d3.max([y[d](115),y[d](y[d].domain()[1])])})
        .attr("x",-15)
        .attr("y", function(d){
            return d3.max([y[d](115),y[d](y[d].domain()[1])])})
        .style("fill", "#feb24c");


    g.append("rect")
        .attr("width",30)
        .attr("height", function(d){return y[d](0) - y[d](100)})
        .attr("x",-15)
        .attr("y", function(d){return y[d](100)})
        .style("fill", "#ddd");

    g.append("rect")
        .attr("width",30)
        .attr("height", function(d){
            return d3.min([y[d](-15),y[d](y[d].domain()[0])]) - y[d](0)
        }).attr("x",-15)
        .attr("y", function(d){
            return y[d](0)
        }).style("fill", "#feb24c");

    g.append("rect")
        .attr("width",30)
        .attr("height", function(d){
            return  y[d](y[d].domain()[0]) - d3.min([y[d](-15),y[d](y[d].domain()[0])])
        }).attr("x",-15)
        .attr("y", function(d){
            return d3.min([y[d](-15),y[d](y[d].domain()[0])])
        }).style("fill", "#e31a1c");



    // Add grey background lines for context.
    background = svg.append("g")
        .attr("class", "background")
        .selectAll("path")
        .data(data)
        .enter().append("path")
        .attr("d", path);


    // Add blue foreground lines for focus.
    foreground = svg.append("g")
        .attr("class", "foreground")
        .selectAll("path")
        .data(data)
        .enter()
        .append("path")
        .attr("d", path);

    svg.append("g")
        .attr("class", "hlabels")
        .selectAll(".labelPath")
        .data(data)
        .enter()
        .append("text")
        .attr("class","labelPath")
        .attr("x", x("2014-11-15") + 25)
        .attr("y", function(d,i) {

            //wbc 320.099173553719
            //rbc 410.19834710743805
            //palatelet 413.2396694214876
            //hemoglobin 323.1404958677686
            //hematocrit 280.94214876033055
            //neutrophils 309.45454545454544
            //lymphocytes 387.76859504132227
            //monocytes 0
            //eosinophils 460
            //basophils 285.12396694214874
            //sodium 114.04958677685948
            //potassium 285.12396694214874
            //chloride 95.04132231404958
            //uricacid 141.4214876033058
            //creatinine 310.59504132231405

            var arr = [0,0,5,6,-5,-7,0,2,0,0,0,8,0,0,0,0,0,0,0,0,0];
            return y["2014-11-15"](d["2014-11-15"]) + arr[i]
        }).text(function(d){
            return d.names;
        }).style("font-size",12);

    // Add an axis and title.
    g.append("g")
        .attr("class", "axis-par")
        .each(function(d) {
            d3.select(this)
                .call(axis.scale(y[d]))
                .selectAll("text")
                .attr("x", -17)
                .style("text-anchor", "end");
        })
        .append("text")
        .style("text-anchor", "middle")
        .attr("y", -9)
        .text(function(d) { return d; });


    // Add and store a brush for each axis.
    g.append("g")
        .attr("class", "brush")
        .each(function(d) { d3.select(this).call(y[d].brush = d3.svg.brush().y(y[d]).on("brush", brush)); })
        .selectAll("rect")
        .attr("x", -8)
        .attr("width", 16);


});

// Returns the path for a given data point.
function path(d) {
    return line(dimensions.map(function(p) { return [x(p), y[p](d[p])]; }));
}

// Handles a brush event, toggling the display of foreground lines.
function brush() {
    var actives = dimensions.filter(function(p) { return !y[p].brush.empty(); }),
        extents = actives.map(function(p) { return y[p].brush.extent(); });
    foreground.style("display", function(d) {
        return actives.every(function(p, i) {
            return extents[i][0] <= d[p] && d[p] <= extents[i][1];
        }) ? null : "none";
    })
}

